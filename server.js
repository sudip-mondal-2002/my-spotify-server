const express = require('express');
const SpotifyWebApi = require('spotify-web-api-node');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
app.use(cors());
app.use(bodyParser.json());
require("dotenv").config();
app.post("/refresh", (req,res)=>{
    const refreshToken = req.body.refreshToken;
    const spotifyApi = new SpotifyWebApi({
        redirectUri: 'https://sudip-mondal-2002.github.io/spotify-client/',
        clientId: '0888184252994a00ab8a1339ed66ad74',
        clientSecret: process.env.SECRET,
        refreshToken: refreshToken,
    })
    spotifyApi.refreshAccessToken()
    .then(data=>{
        res.json({
            accessToken: data.body["access_token"],
            expiresIn: data.body["expires_in"],
        })
    })
    .catch(err =>{
        res.sendStatus(400)
    });

})
app.post("/login", (req, res) => {
    const code = req.body.code
    const spotifyApi = new SpotifyWebApi({
        redirectUri: 'https://sudip-mondal-2002.github.io/spotify-client/',
        clientId: '0888184252994a00ab8a1339ed66ad74',
        clientSecret: process.env.SECRET,
    })
    spotifyApi
        .authorizationCodeGrant(code)
        .then(data => {
            res.json({
                accessToken: data.body.access_token,
                refreshToken: data.body.refresh_token,
                expiresIn: data.body.expires_in,
            })
        })
        .catch(err => {
            res.sendStatus(400)
        })
})
PORT = process.env.PORT || 8000
app.listen(PORT,()=>{
    console.log("listening on port", PORT)

});